package course.maven;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Pen myPen = new Pen (1, 1, "RED");
        System.out.println("color " + myPen.getColor());
        Pen myPen2 = new Pen (0,1, "BLUE");
        System.out.println("color " + myPen2.getColor());
    }
}
